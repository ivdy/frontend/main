import React from 'react';
import './App.scss';

function App() {
  const cards = [
    {
      img: "heart.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "infectious-disease.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "people.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "money.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "light-bulb.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "accountant.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "estate-agent.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },
    {
      img: "computer.png",
      heading: "Aliquam Tellus Ex",
      body: "Mauris pharetra augue ut ligula molestie sodales"
    },

  ]


  return (
    <div className="App">
      <div className="nav-wrapper">
        <nav>
          <div className="logo">
            <img src="/images/logo-header.png" alt="ivdy logo" />
          </div>
          <ul className="links">
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/">About Us</a>
            </li>
            <li>
              <a href="/">Services</a>
            </li>
            <li>
              <a href="/">Careers</a>
            </li>
          </ul>
          <button>Login</button>
        </nav>
      </div>

      <header>
        <div className="backdrop">
          <div></div>
          <div></div>
        </div>

        <div className="row hero">
          <div className="sm-12  md-8 text">

            <h1>Features</h1>
            <p>
              Sed placerat mauris varius non. Curabitur dui nibh,
              semper vitae pulvinar eget, molestie ut diam.
            </p>
          </div>
          <div className="sm-12 md-4 graphic">
            <img src="/images/graphic-header.png" alt="header graphic" />
          </div>
        </div>
      </header>

      <main>
        <section className="uses">
          <div className="text--center">
            <p className="text--primary">Use Cases</p>
            <h3>Dolor Sit Amet, <span className="text--primary">Consecteur</span> Adispicing Elit.</h3>
          </div>

          <div className="row cards">
            {cards.map((card, index) => (
              <div className="col sm-6 md-4 lg-3" key={index}>
                <div className="card">
                  <img src={`/images/${card.img}`} alt="" />
                  <h3>{card.heading}</h3>
                  <span>{card.body}</span>
                </div>
              </div>
            ))}
          </div>
        </section>

        <section className="infographic">
          <div className="row container">
            <div className="col sm-12 md-4 graphic">
              <img src="/images/graphic-mid-2.png" alt="some usefult alt" />
            </div>
            <div className="col sm-12 md-8 text">
              <h3>Aenean Non Viverra Ante Cras Nec Odio A Nisi Convallis</h3>
              <p>
                Mauris pharetra augue ut ligula molestie sodales. Aliquam malesuada congue rutrum. Suspendisse velit metus, sollicitudin ac ante sit amet, placerat tincidunt odio. Sed tempus aliquet ante, ac venenatis elit dapibus in. Cras at consectetur dolor. Donec at nibh ut massa fermentum aliquet at quis metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p>
            </div>
          </div>
          <div className="row container">
            <div className="col sm-12 md-4 graphic">
              <img src="/images/graphic-mid.png" alt="some usefult alt" />
            </div>
            <div className="col sm-12 md-8 text">
              <h3>Donec Hendrerit <span className="text--primary">Suscipit</span> Ligula Eu Viverra.</h3>
              <p>
                Mauris pharetra augue ut ligula molestie sodales. Aliquam malesuada congue rutrum. Suspendisse velit metus, sollicitudin ac ante sit amet, placerat tincidunt odio. Sed tempus aliquet ante, ac venenatis elit dapibus in. Cras at consectetur dolor. Donec at nibh ut massa fermentum aliquet at quis metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p>
            </div>
          </div>
          <div className="row container">
            <div className="col sm-12 md-4 graphic">
              <img src="/images/graphic-bottom.png" alt="some usefult alt" />
            </div>
            <div className="col sm-12 md-8 text">
              <h3>Aenean Non Viverra Ante Cras Nec Odio A Nisi Convallis</h3>
              <p>
                Mauris pharetra augue ut ligula molestie sodales. Aliquam malesuada congue rutrum. Suspendisse velit metus, sollicitudin ac ante sit amet, placerat tincidunt odio. Sed tempus aliquet ante, ac venenatis elit dapibus in. Cras at consectetur dolor. Donec at nibh ut massa fermentum aliquet at quis metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              </p>
            </div>
          </div>

        </section>
      </main>

      <footer>
        <section className="row prefooter">
          <div className="sm-6 md-3">
            <img src="/images/logo-footer.png" alt="ivdy logo" />
            <h3>Follow Us</h3>
            <div className="socials">
              <div><img src="/images/facebook-icon.png" alt="facebook icon" /></div>
              <div><img src="/images/instagram-icon.png" alt="instagram icon" /></div>
              <div><img src="/images/linkedin-icon.png" alt="linkedin icon" /></div>
            </div>
          </div>
          <div className="sm-6 md-3">
            <h3>Quick Links</h3>
            <ul>
              <li>Home</li>
              <li>About Us</li>
              <li>Services</li>
              <li>Career</li>
              <li>Contact</li>
            </ul>
          </div>
          <div className="sm-6 md-3">
            <h3>Contact</h3>
            <div>
              <div>
                <img src="/images/phone.png" alt="phone icon" />
                <p>+23145699874</p>
              </div>
              <div>
                <img src="/images/email.png" alt="email icon" />
                <p>ivdy@webmail.com</p>
              </div>
            </div>
          </div>
          <div className="sm-6 md-3">
            <h3>Subscribe To News Update</h3>
            <div className="newsletter">
              <input type="email" placeholder="Email" name="email" id="email" />
              <button className="text--primary">Subscribe</button>
            </div>
          </div>
        </section>
        <div className="footer">
          <span>Terms &amp; Conditons</span>
          <span>&copy; ivdy2021</span>
          <span>Privacy Policy</span>
        </div>
      </footer>
    </div>
  );
}

export default App;
